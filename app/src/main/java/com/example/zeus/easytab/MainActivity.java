package com.example.zeus.easytab;

import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import com.example.zeus.easytab.ne;
import goldzweigapps.tabs.Builder.EasyTabsBuilder;
import goldzweigapps.tabs.EasyTabsColors;
import goldzweigapps.tabs.transforms.EasyZoomOutSlideTransformer;

public class MainActivity extends AppCompatActivity {

    EasyTabsBuilder builder;
    EasyTabsColors colors;
    TabLayout tabs;
    ViewPager pager;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        colors = new EasyTabsColors();
        tabs = (TabLayout)findViewById(R.id.tabs);
        pager = (ViewPager)findViewById(R.id.pager);
        builder = new EasyTabsBuilder(this, tabs, pager);
        builder=new EasyTabsBuilder(this,tabs,pager).addTabs(false,new goldzweigapps.tabs.Builder.TabItem(new ne(),"alpha"),
                new goldzweigapps.tabs.Builder.TabItem(new ne(),"alpha"),
                new goldzweigapps.tabs.Builder.TabItem(new ne(),"alpha"),
                new goldzweigapps.tabs.Builder.TabItem(new ne(),"alpha")).setTransformation(true,new EasyZoomOutSlideTransformer());

    }
}
